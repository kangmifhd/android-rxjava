package com.mifhd.rxjava.util;

import android.util.Log;

import com.mifhd.rxjava.BuildConfig;

public class Logger
{
    private static final int STACK_TRACE_LEVELS_UP = 5;
    private static final String SEPARATOR = ", ";

    /**
     * Log debug with dynamic string argument and automatically separate it with certain char SEPARATOR
     * get class name, method name, line number, messages
     *
     * @return String - Class name, method name, Current line number, log message, log mesaage, log message...
     */
    public static void d(String ...message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.d("D>>:"+getClassNameMethodNameAndLineNumber(), getMessage(message));
        }
    }

    public static void t(Throwable tr)
    {
        if (BuildConfig.DEBUG)
        {
            Log.e("Throwable>>:", getClassNameMethodNameAndLineNumber(), tr);
        }
    }

    /**
     * Log verbose with dynamic string argument and automatically separate it with certain char SEPARATOR
     * get class name, method name, line number, messages
     *
     * @return String - Class name, method name, Current line number, log message, log mesaage, log message...
     */
    public static void v(String ...message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.v("V>>:"+getClassNameMethodNameAndLineNumber(), getMessage(message));
        }
    }

    /**
     * Log info with dynamic string argument and automatically separate it with certain char SEPARATOR
     * get class name, method name, line number, messages
     *
     * @return String - Class name, method name, Current line number, log message, log mesaage, log message...
     */
    public static void i(String ...message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.i("I>>:"+getClassNameMethodNameAndLineNumber(), getMessage(message));
        }
    }

    /**
     * Log warn with dynamic string argument and automatically separate it with certain char SEPARATOR
     * get class name, method name, line number, messages
     *
     * @return String - Class name, method name, Current line number, log message, log mesaage, log message...
     */
    public static void w(String ...message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.w("W>>:"+getClassNameMethodNameAndLineNumber(), getMessage(message));
        }
    }

    /**
     * Log error with dynamic string argument and automatically separate it with certain char SEPARATOR
     * get class name, method name, line number, messages
     *
     * @return String - Class name, method name, Current line number, log message, log mesaage, log message...
     */
    public static void e(String ...message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.e("E>>:"+getClassNameMethodNameAndLineNumber(), getMessage(message));
        }
    }

    /**
     * Log error with dynamic string argument and automatically separate it with certain char SEPARATOR
     * get class name, method name, line number
     *
     * @return String - Class name, method name, Current line number, log message, log mesaage
     */
    private static String getMessage(String[] message)
    {
        if (BuildConfig.DEBUG)
        {
            String str = "";
            for (String m: message){
                str += m + SEPARATOR;
            }
            return str.replaceAll(SEPARATOR +"$", "");
        }
        return null;
    }

    /**
     * Get the current line number. Note, this will only work as called from
     * this class as it has to go a predetermined number of steps up the stack
     * trace. In this case 5.
     *
     * @author kvarela
     * @return int - Current line number.
     */
    private static int getLineNumber()
    {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getLineNumber();
    }

    /**
     * Get the current class name. Note, this will only work as called from this
     * class as it has to go a predetermined number of steps up the stack trace.
     * In this case 5.
     *
     * @author kvarela
     * @return String - Current line number.
     */
    private static String getClassName()
    {
        String fileName = Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getFileName();

        // kvarela: Removing ".java" and returning class name
        return fileName.substring(0, fileName.length() - 5);
    }

    /**
     * Get the current method name. Note, this will only work as called from
     * this class as it has to go a predetermined number of steps up the stack
     * trace. In this case 5.
     *
     * @author kvarela
     * @return String - Current line number.
     */
    private static String getMethodName()
    {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName();
    }

    /**
     * Returns the class name, method name, and line number from the currently
     * executing log call in the form .()-
     *
     * @author kvarela
     * @return String - String representing class name, method name, and line
     *         number.
     */
    private static String getClassNameMethodNameAndLineNumber()
    {
        return getClassName() + ":" + getLineNumber() + " -> " + getMethodName() + "()";
    }

}
